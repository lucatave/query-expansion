# Query Expansion

## Overview

This software's purpose is to expand a query with the user's interests.

## Requirements

* [docker](https://docs.docker.com/engine/installation/)

* [docker-compose](https://docs.docker.com/compose/install/)

## Installation

- Clone the repository

```bash
git clone https://github.com/lucatave/query-expansion.git 
```

- Go into the downloaded repository

```bash
cd query-expansion
```

- Then build the docker container

```bash
docker-compose build
```


## Usage 

- Now we can run the help container

```bash
docker-compose run --rm app-help
```

Before using the app, we need to create a new db instance as shown in help

```bash
docker-compose run --rm app-create
```

- Then we can calculate the social page rank needed to expand a query

```bash
docker-compose run --rm app-spr
```

- Finally to expand a query we can use the app service or the app-query service

```bash
docker-compose run --rm app -q "query"
```

- Follows the app-query service

```bash
docker-compose run --rm app-query "query"
```
